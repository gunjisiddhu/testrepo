package org.epam.dscourse;

import java.util.Arrays;
import java.util.Comparator;

public class StudentSort {

    public Student[] sortStudentsByGradeAndId(Student[] students) {
        Arrays.sort(students, Comparator.comparing(Student::grade).thenComparing(Student::id));
        return students;
    }
}
