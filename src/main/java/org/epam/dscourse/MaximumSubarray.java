package org.epam.dscourse;

public class MaximumSubarray {
    /**
     * <a href="https://leetcode.com/problems/maximum-subarray/description/">Autocode -> Maximum Subarray</a>
     * <a href="https://autocode-next.lab.epam.com/courses/1379/syllabus/5686">Leetcode</a>
     */
    public int maxSubArray(int[] nums) {
        if(nums == null || nums.length==0)
            return 0;
        int maxSum = nums[0];
        int currentSum = nums[0];
        for(int i=1;i<nums.length;i++){
            currentSum += nums[i];
            if(currentSum>maxSum)
                maxSum = currentSum;
            if(currentSum<0)
                currentSum = 0;
        }
        return maxSum;
    }
}
