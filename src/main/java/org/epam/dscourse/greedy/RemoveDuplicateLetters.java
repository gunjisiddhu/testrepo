package org.epam.dscourse.greedy;

public class RemoveDuplicateLetters {

    public String removeDuplicateLetters(String s) {
        if(s==null || s.length()==1)
            return null;
        int[] counter = new int[26];
        int[] visited = new int[26];
        int n = s.length();
        for (int i = 0; i < n; i++)
            counter[s.charAt(i) - 'a']++;
        String res = "";
        for (int i = 0; i < n; i++) {
            counter[s.charAt(i) - 'a']--;
            if (visited[s.charAt(i) - 'a'] == 0) {
                int size = res.length();
                while (size > 0 &&
                        res.charAt(size - 1) > s.charAt(i) &&
                        counter[res.charAt(size - 1) - 'a'] > 0) {

                    // Mark letter unvisited
                    visited[res.charAt(size - 1) - 'a'] = 0;
                    res = res.substring(0, size - 1);
                    size--;
                }
                res += s.charAt(i);
                visited[s.charAt(i) - 'a'] = 1;
            }


        }
        return res;
    }
}
