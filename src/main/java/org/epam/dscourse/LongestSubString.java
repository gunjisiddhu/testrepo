package org.epam.dscourse;

import java.util.Arrays;

public class LongestSubString {

    public boolean isValidSubString(String str, int count){
        int[] counter = new int[26];
        Arrays.fill(counter,0);
        char[] ch = str.toCharArray();
        for (char c : ch) {
            counter[c-'a']++;
        }
        for(int i=0;i<26;i++) {
            if (counter[i] != 0 && counter[i] < count)
                return false;
        }
        return true;
    }


    public int longestSubstring(String str, int k) {
        if(str==null || str.length()==0)
            return 0;
        if(k>= str.length())
            return 0;
        if(k<=1)
            return str.length();

        int maxSoFar =  1;
        char[] ch = str.toCharArray();
        for(int i=0;i<ch.length;i++){
            if(isValidSubString(new String(ch,0,i+1),k))
                maxSoFar = i+1;
        }
        System.out.println(maxSoFar);
        return maxSoFar;
    }


}
