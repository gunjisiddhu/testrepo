package org.epam.dscourse.dp;

public class MaxLengthOfRepeatedSubArray {

    public int findLength(int[] nums1, int[] nums2) {
        if(nums1==null || nums2 ==null || nums1.length==0 || nums2.length==0)
            return -1;
        ///complete the code with dynamic programming
        int maxNumber = 0;
        int dpTable[][] = new int[nums1.length+1][nums2.length+1];
        int n = nums2.length;
        int r = nums1.length;
        for(int i= 1;i<=n;i++){
            for(int j=1;j<=r;j++){
                if(nums1[i-1]==nums2[j-1]){
                    dpTable[i][j] = 1+dpTable[i-1][j-1];
                    if(dpTable[i][j]>maxNumber)
                        maxNumber = dpTable[i][j];
                }
            }
        }


        return maxNumber;
    }



    public int minCost(int[][] costs) {
        if(costs.length == 0) {
            return 0;
        }
        int c0 = 0;
        int c1 = 0;
        int c2 = 0;
        int k0 = 0;
        int k1 = 0;
        int k2 = 0;

        for (int i = 0; i < costs.length; i++) {
            c0 = costs[i][0] + Math.min(k1, k2);
            c1 = costs[i][1] + Math.min(k0, k2);
            c2 = costs[i][2] + Math.min(k0, k1);
            k0 = c0;
            k1 = c1;
            k2 = c2;
        }

        return Math.min(Math.min(k0, k1), k2);
    }
}



