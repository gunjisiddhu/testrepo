package org.epam.dscourse;


public class CreatingStringFromDI {

    private String createString(char[] position,int[] frequency, String currentString, char incOrDec,int currentIndex, int maxLength){
        if(currentIndex == maxLength)
            return currentString;
        if(currentIndex == 0){
            for(int i=1;i<=9;i++){
                frequency[i]++;
                String finalOutput = createString(position,frequency,currentString+i,position[currentIndex],currentIndex+1,maxLength);
                if(finalOutput.length() == maxLength)
                    return finalOutput;
                frequency[i]--;
            }
        }else{
            int prevIndexValue = currentString.toCharArray()[currentIndex-1]-'0';
            if(incOrDec == 'I'){
                for(int i=prevIndexValue+1;i<=9;i++){
                    String finalOutput = getString(position, frequency, currentString, currentIndex, maxLength, i);
                    if (finalOutput != null) return finalOutput;
                }
            }else{
                for(int i=prevIndexValue-1;i>=1;i--){
                    String finalOutput = getString(position, frequency, currentString, currentIndex, maxLength, i);
                    if (finalOutput != null) return finalOutput;
                }

            }
        }
        return currentString;
    }

    private String getString(char[] position, int[] frequency, String currentString, int currentIndex, int maxLength, int i) {
        if(frequency[i]<1){
            frequency[i]++;
            String finalOutput = "";
            if(currentIndex == position.length){
                finalOutput = createString(position,frequency,currentString+i,'I',currentIndex+1,maxLength);
            }else{
                finalOutput = createString(position,frequency,currentString+i,position[currentIndex],currentIndex+1,maxLength);
            }
            if(finalOutput.length() == maxLength)
                return finalOutput;
            frequency[i]--;
        }
        return null;
    }


    public String constructSmallestNumberFromDIString(String input) {
        if(input.length() == 0)
            return null;

        return createString(input.toCharArray(),new int[10],"",'I', 0, input.length()+1);
    }

}
