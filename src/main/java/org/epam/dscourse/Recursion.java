package org.epam.dscourse;

public class Recursion {
    /**
     * Given a number n, find the factorial of the number using recursion.
     * @param n
     * @return
     */
    public int factorial(int n) {
        if(n<0)
            return factorial(-1*n);
        if(n==1 || n==0)
            return 1;
        else
            return n*factorial(n-1);
    }

    /**
     * Given an array of integers, find the sum of all the integers using recursion.
     * if the input is invalid return 0.
     * @param input
     * @return
     */
    public int sumOfIntegers(int[] input) {
        if(input==null || input.length == 0)
            return 0;
        return findSum(input,input.length-1);
    }

    public int findSum(int[] arr, int index){
        if(index<0)
            return 0;
        else
            return arr[index]+findSum(arr, index-1);
    }

}