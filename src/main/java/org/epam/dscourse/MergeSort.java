package org.epam.dscourse;

import java.util.HashMap;

public class MergeSort implements AutoCloseable{


    public int findKthLargest(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k > nums.length)
            throw new IllegalArgumentException();
        split(nums, 0, nums.length - 1);
        return nums[nums.length - k];

    }


    private int[] join(int[] arr, int i, int mid, int n) {
        int currIndex = i;
        int left = i;
        int right = mid + 1;
        int[] dummyArray = new int[arr.length];
        while (left <= mid && right <= n) {
            if (arr[left] < arr[right]) {
                dummyArray[currIndex] = arr[left];
                left++;
            } else {
                dummyArray[currIndex] = arr[right];
                right++;
            }
            currIndex++;
        }
        while (left <= mid) {
            dummyArray[currIndex] = arr[left];
            left++;
            currIndex++;
        }

        while (right <= n) {
            dummyArray[currIndex] = arr[right];
            right++;
            currIndex++;
        }


        for (int k = i; k < currIndex; k++) {
            arr[k] = dummyArray[k];
        }
        return arr;
    }

    private int[] split(int[] arr, int i, int n) {
        if (i < n) {
            int mid = (i + n) / 2;
            split(arr, i, mid);
            split(arr, mid + 1, n);
            join(arr, i, mid, n);
        }

        return arr;
    }


    public int[] sortArray(int[] nums) {
        if (nums == null || nums.length == 0)
            throw null;
        split(nums, 0, nums.length - 1);
        return nums;
    }


    @Override
    public void close() {

    }
}



