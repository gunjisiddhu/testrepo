package org.epam.dscourse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class PhoneNumber {
    HashMap<Integer, char[]> letterCount;
    List<String> finalList;


    private void addStringToList(char[] digits, String currentString, int currentIndex, int targetSize){
        if(currentIndex == targetSize){
            finalList.add(currentString);
        }else{
            switch (digits[currentIndex]){
                case '2':
                    for (char c : letterCount.get(2)) {
                        addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                    }
                    break;
                case '3':
                    for (char c : letterCount.get(3)) {
                        addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                    }
                    break;
                case '4':
                    for (char c : letterCount.get(4)) {
                        addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                    }
                    break;
                case '5':
                    for (char c : letterCount.get(5)) {
                        addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                    }
                    break;
                case '6':
                    for (char c : letterCount.get(6)) {
                        addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                    }
                    break;
               case '7':
                   for (char c : letterCount.get(7)) {
                       addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                   }
                    break;
               case '8':
                   for (char c : letterCount.get(8)) {
                       addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                   }
                    break;
               case '9':
                   for (char c : letterCount.get(9)) {
                       addStringToList(digits, currentString+c, currentIndex+1, targetSize);
                   }
                    break;
                default:
                    break;
            }

        }
    }

    public List<String> letterCombinations(String digits) {
        if(digits==null || digits.length() == 0)
            return Collections.emptyList();

        letterCount = new HashMap<>();
        letterCount.put(2,new char[]{'a','b','c'});
        letterCount.put(3,new char[]{'d','e','f'});
        letterCount.put(4,new char[]{'g','h','i'});
        letterCount.put(5,new char[]{'j','k','l'});
        letterCount.put(6,new char[]{'m','n','o'});
        letterCount.put(7,new char[]{'p','q','r','s'});
        letterCount.put(8,new char[]{'t','u','v'});
        letterCount.put(9,new char[]{'w','x','y','z'});

        finalList = new ArrayList<>();
        addStringToList(digits.toCharArray(), "", 0, digits.length());
        return finalList;
    }


}
