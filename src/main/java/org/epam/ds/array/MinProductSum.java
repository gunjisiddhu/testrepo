package org.epam.ds.array;

import java.util.*;

public class MinProductSum {
    public int minProductSum(int[] nums1, int[] nums2) {
        if(nums1==null || nums2 == null)
            return 0;

        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int product = 0;
        for(int i=0;i<nums1.length;i++){
            product += nums1[i]*nums2[nums2.length-1-i];
        }

        return product;
    }
}
