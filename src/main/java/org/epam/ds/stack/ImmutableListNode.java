package org.epam.ds.stack;

public interface ImmutableListNode {
    public int printValue(); // print the value of this node.
    public ImmutableListNode getNext(); // return the next node.
}
