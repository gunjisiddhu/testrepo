package org.epam.ds.stack;

import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class StackImpl {
    public int[] printLinkedListInReverse(ImmutableListNode head) {
        if(head == null)
            return new int[]{};
        Stack<Integer> stack = new Stack<>();
        do {
            stack.push(head.printValue());
            head = head.getNext();
        } while (head != null);
        int[] returnArray = new int[stack.size()];
        int i = 0;
        while(!stack.isEmpty()){
            returnArray[i++] = stack.pop();
        }
        return returnArray;
    }

    public int[] deckRevealedIncreasing(int[] deck) {
        if(deck==null || deck.length==0)
            return new int[]{};

        int n = deck.length;
        Deque<Integer> index = new LinkedList<>();
        for (int i = 0; i < n; ++i)
            index.add(i);

        int[] ans = new int[n];
        Arrays.sort(deck);
        for (int card: deck) {
            ans[index.pollFirst()] = card;
            if (!index.isEmpty())
                index.add(index.pollFirst());
        }

        return ans;
    }
}
