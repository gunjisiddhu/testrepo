package org.epam.ds.heap;

import java.io.IOException;
import java.util.*;

public class StickCombining {
    public int connectSticks(int[] sticks) {
        if(sticks == null || sticks.length <= 1)
            return 0;
        List<Integer> treeSet = new ArrayList<>();
        for(int i:sticks)
            treeSet.add(i);
        Collections.sort(treeSet);
        int finalLength = 0;
        while(treeSet.size()>1){
            int a = treeSet.get(0);
            treeSet.remove(0);
            int b = treeSet.get(0);
            treeSet.remove(0);
            finalLength += (a+b);
            treeSet.add((a+b));
            Collections.sort(treeSet);
        }
        return finalLength;
    }
}
