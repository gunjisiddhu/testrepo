package org.epam.ds.tree;


public class LeavesSum {

    int treeHeight(TreeNode root){
        if(root == null)
            return 0;
        int leftHeight = treeHeight(root.getLeft());
        int rightHeight = treeHeight(root.getRight());
        if(leftHeight>=rightHeight)
            return leftHeight+1;
        else
            return rightHeight+1;

    }

    int leaveSum(TreeNode root, int maxHeight, int currentHeight){
        if(root == null)
            return 0;
        if(root.getLeft()==null && root.getRight()==null){
            if(currentHeight == maxHeight)
                return root.getValue();
            else
                return 0;
        }
        return leaveSum(root.getLeft(), maxHeight, currentHeight+1)+leaveSum(root.getRight(), maxHeight, currentHeight+1);
    }


    public int deepestLeavesSum(TreeNode root) {
        if(root == null)
            return 0;
        return leaveSum(root,treeHeight(root), 0);
    }
}
